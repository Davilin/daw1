#Imprimo en pantalla de que va el programa
print("A continuación escribe tu fecha de nacimiento para saber tu número de la suerte") 
#Creo una variable de entrada del usuario para que ponga su día de nacimiento
dia=int(input("Introduce el día="))
#Creo un bucle para la variable día que si es menor a 0 o mayor a 31 de error
while dia<=0 or dia >=31:
    print("El día es incorrecto")
    dia=int(input("Introduce un día en formato DD="))
#una vez es correcto el día, creo una variable para introducir el mes
mes=int(input("Ahora introduce el mes="))
#Creo un bucle para la variable mes que si es menor a 0 o mayor a 12 de error
while mes<=0 or mes >12:
    print("El mes es incorrecto")
    mes=int(input("Introduce el mes en formato MM="))
#Una vez el mes es correcto, creo la variable para introducir el año
ano=int(input("Ahora introduce el año="))
#He creado un bucle con una ventana de tiempo, sólo pueden introducir años desde 1900 hasta 2022
while ano <=1900 or ano >2022:
    print("El año es incorrecto")
    ano=int(input("Ahora introduce el año en formato AAAA="))
#Con todos los datos imprimo en pantalla la fecha que ha introducido y, aquí utilizo .rjust para que aparezcan los 0 a la izda.
print("Tu fecha de nacimiento es el", dia,"/", mes, "/", ano, ". Que unido es", str(dia).rjust(2,"0")+ str(mes).rjust(2,"0")+str(ano))
#Creo la variable completo para que sea una cadena de texto seguida
completo=str(dia).rjust(2,"0")+ str(mes).rjust(2,"0")+str(ano)
#Creo la variable completo int, porque lo paso a entero
completoent=int(completo)
#creo la variable x para acumular la suma del 1er bucle for

x=0
#Creo la variable lucky_number para acumular la suma del 2do bucle for
#Creo el bucle por con 9 vueltas, :D, con la variable i de for, busco el módulo y se lo sumo a x, así 8 veces
for i in range (1,8+1):
    i=completoent%10
    x=x+i
#con la división entera entre 10 consigo reducir la unidad anterior y se la vuelvo a asignar a completoent, así 8 veces.
    completoent=completoent//10    
#Imprimo en pantalla información de lo que haré
print ("Después de sumar los números.....")
#Creo 2 variables para sumar en los siguientes bucles
y=0
z=0
#Esta parte me ha costado, ufff!!
#Creo un bucles, mientras x sea mayor de 10 y con la condición que y sea menor de 9 que haga la fórmula
while x>=10:
    if x>=10 and y<=9:
        for j in range (1,2+1):
            j=x%10
            y=y+j
            x=x//10
#creo la condicion que solo imprima si x e y son ya 1 único dígito
    if x<=10 and y<=10:
        print("Tu número de la suerte es el=",y)
#Si todavia no es un único dígito significa que y es mayor a 10
#vuelvo a crear un bucle para obtener 1 único dígito
while y>=10:
    if y>=10 and z<=9:
        for k in range (1,2+1):
            k=y%10
            z=z+k
            y=y//10

#Creo la condición que sólo imprima z cuando "y" sea menor a 10.
    if y<=10:
        print ("Tu número de la suerte es el=",z)

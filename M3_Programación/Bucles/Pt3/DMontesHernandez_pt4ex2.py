"""Calculeu i mostreu el resultat de l’operació a b on la base a i l'exponent b són valors que
l'usuari introdueix per pantalla. El programa no continuarà fins que els nombres que
s’introdueixin pel teclat siguin positius. Considereu la base i l'exponent de tipus sencer
i positius. El que heu de fer és multiplicar a (base) tantes vegades com us diu el nombre b
(exponent)."""
#Imprimo en pantalla lo que hará el usuaio
print ("A continución introduce un número y su exponente")
#Le solicito al usuario la base, en número entero, para la operación
a=int(input("Introduce el número base"))
#Creo un bucle para que si es menor a 0 le vuelva a pedir el número y sea un número positivo
while a<0:
    print ("Debes elegir un número positivo")
    a=int(input("Introduce el número base"))
#Si pone el número positivo, entonces le pide el exponente.
b=int(input("Ahora introduce el exponente="))
#Creo un bucle para que el exponente tampoco sea negativo
while b<0:
    print ("Para el exponente, debes elegir un número positivo")
    b=int(input("Introduce el número exponente"))
#aplico la fórmula para elevar la base por el número introducido.
print("El resultado de la operación es=",a**b)